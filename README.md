### Dear customers of Cloudflare,

if you serve your website through Cloudflare, then chances are good that
[Tor](https://www.torproject.org/projects/torbrowser.html.en) users visiting
your site will be greeted with a **bothersome captcha** like this:

![](captcha.png)![](captcha2.png)

Meeting one or two of these captchas in the course of a day is no big deal;
having to deal with tens or even hundreds of them is. They significantly detract
from the browsing experience.

These captchas are particularly annoying because your humanity is only
recognized if you allow cookies – if you don't, then after solving the captcha
you are swiftly presented with a new one – and because you have to re-prove
your humanity every quarter of an hour. These captchas also [help attackers
de-anonymize Tor traffic](https://news.ycombinator.com/item?id=12122268).

**Please consider making your site friendly and welcoming for Tor users.**
You as a Cloudflare customer can change this behavior for your site. Tor users
are grouped as a virtual "country" for which you can override the settings
without influencing the treatment of non-Tor users. [This Cloudflare support
page explains how this can be done.](https://support.cloudflare.com/hc/en-us/articles/203306930-Does-Cloudflare-block-Tor-)

**Thank you,** ❤︎ <br>
a Tor user

**Discussion:** on [Hacker News](https://news.ycombinator.com/item?id=17750801) and at
[this issue](https://gitlab.com/iblech/tor-appeal/issues/1).
